package com.company;

import java.util.Arrays;

/** Homework06_procedure
 * Задание:
 * Реализовать процедуру, которая переместит все
 * значимые элементы влево, заполнив нулевые, например:
 * было:
 *  34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
 * стало:
 * 34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
 */

public class Main {

    private static int[] anNotOrderArray;

    static {
        anNotOrderArray = new int[]{34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
    }

    /**
     * Процедура упорядывачивает массив перемещяя
     * значащие цифры в левую часть
     * а нули в правую
     */
    public static void getNotOrderingArray() {
        int i = 0;
        while (i < anNotOrderArray.length) {
            if (anNotOrderArray[i] == 0) {
                int j = i  ;
                while (j < anNotOrderArray.length) {
                    if (anNotOrderArray[j] > 0) {
                        anNotOrderArray[i] = anNotOrderArray[j];
                         i = i + 1;
                        anNotOrderArray[j] = 0;
                    }
                    j++;
                }
            }
            i++;
        }
    }

        public static void main (String[]args){
        
            System.out.println("Массив до: " + Arrays.toString(anNotOrderArray));
            getNotOrderingArray();
            System.out.println("Массив после: " + Arrays.toString(anNotOrderArray));
        }
}

