package ru.kochetov_homeworks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryFileImpl implements UserRepository {

    private String fileName;

    public UserRepositoryFileImpl(String fileName) {

        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {

        List<User> users = new ArrayList<>();
        FileService f = new FileService();
        FileReader reader = f.OpenFileForReading(fileName);
        BufferedReader bufferedReader = f.OpenBufferedReader(reader);
        LineService l = new LineService();

        String line = l.LineFromReader(bufferedReader);

        while (line != null) {
            String[] parts = line.split("\\|");
            String name = parts[0];
            int age = Integer.parseInt(parts[1]);
            boolean isWorker = Boolean.parseBoolean(parts[2]);
            User newUser = new User(name, age, isWorker);
            users.add(newUser);
            line = l.LineFromReader(bufferedReader);
        }
        f.closeBufferedReader(bufferedReader);
        f.closeFileForReading(reader);
        return users;
    }

    @Override
    public void save(User user) {

        FileService f = new FileService();
        FileWriter writer = f.OpenFileForWriting(fileName);
        BufferedWriter bufferedWriter = f.OpenBufferedWriter(writer);
        String stringToFile = (user.getName() + "|" + user.getAge() + "|" + user.getIsWorker());
        LineService l = new LineService();
        l.LineToFile(bufferedWriter, stringToFile);
        f.closeBufferedWriting(bufferedWriter);
        f.closeFileForWriting(writer);
    }


    @Override
    public List<User> findByAge(List<User> users, int age) {
        ArrayList<User> usersByAge = new ArrayList();
        for (User u : users){
            if (u.getAge() == age) {
                usersByAge.add(u);
            }

        }

        return usersByAge;
    }

    @Override
    public List<User> findByIsWorkerIsTrue(List<User> users, boolean isWorks) {
        ArrayList<User> usersByWork = new ArrayList();
        for (User u : users) {
            if (u.getIsWorker() == isWorks) {
                usersByWork.add(u);
            }

        } return usersByWork;
    }
}
