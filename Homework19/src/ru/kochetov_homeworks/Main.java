package ru.kochetov_homeworks;

/*
    Задание

    Реализовать методы UsersRepository

 */

// Решение


import java.util.ArrayList;
import java.util.List;

public class Main {

    (public static void main(String[] args)) {

        String filePath = "/home/yo/Git/kochetov_homeworks/Homework19/src/ru/kochetov_homeworks/";
        String fileUsers = "users.txt";
        UserRepository userRepository = new UserRepositoryFileImpl(filePath + fileUsers);
        List<User> users = userRepository.findAll();
        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.getIsWorker());
        }
        System.out.println("Конец списка user.txt");

        User user = new User("Лёша", 33, true);
        userRepository.save(user);
        System.out.println("Конец добавления ползователя в user.txt");

        List<User> usersToSearchByAge = userRepository.findAll();
        int ageToSeach = 15;
        List<User> usersAge = userRepository.findByAge(usersToSearchByAge, ageToSeach);
        if (usersAge.size() > 0)
            for (User userA : usersAge) {
                System.out.println(userA.getName() + " " + userA.getAge());
            }
        else {
            System.out.println("Пользователи с возрастом " + ageToSeach + " не найдены");
        }

        List<User> usersToSearchByWork = userRepository.findAll();
        boolean userIsWork = true;
        List<User> usersToWork = userRepository.findByIsWorkerIsTrue(usersToSearchByWork, userIsWork);
        if (usersToWork.size() > 0)
            for (User userW : usersToWork) {
                System.out.println(userW.getIsWorker() + " " + userW.getName());
            }
        else {
            System.out.println("Работающие пользователи  не найдены");
        }

    }
}
