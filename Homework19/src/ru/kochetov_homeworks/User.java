package ru.kochetov_homeworks;

public class User {
    private String name;
    private int age;
    private boolean isWorker;
    public User(String name, int age, boolean isWorker){
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }
    public String  getName(){
        return this.name;
    }

    public int getAge() {
        return this.age;
    }
    public boolean getIsWorker(){
        return this.isWorker;
    }

}
