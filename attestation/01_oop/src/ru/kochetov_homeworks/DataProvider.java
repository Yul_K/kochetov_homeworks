package ru.kochetov_homeworks;

public class DataProvider {

    private String filePath = "src/ru/kochetov_homeworks/";
    private String fileUsers = "user.txt";
    private int userID = 1;
    private String userNameForReplace = "Марсель";
    private int userAgeForReplace = 27;
    private int indexIDField = 0;
    
    public String getFileDB() {return (filePath + fileUsers);}
    public int getUserID() {return userID;}
    public String getUserNameForReplace() {return userNameForReplace;}
    public int getUserAgeForReplace() {return userAgeForReplace;}
    public int getIndexIDField() {return indexIDField;}
}