package ru.kochetov_homeworks;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    public void printAllUsers(String dataFile) {

        UserRepository userRepository = new UserRepositoryFileImpl(dataFile);
        List<User> users = userRepository.findAll();
        for (User user : users) {
            System.out.println(user.getId() + " " + user.getName() + " " + user.getAge() + " " + user.getIsWorker());
        }
        System.out.println("Конец списка user.txt");
    }

    public void addNewUser(String dataFile) {
        // TODO сделать добавление пользователя из  файла
        User user = new User(50,"Лёша", 33, true);
        UserRepository userRepository = new UserRepositoryFileImpl(dataFile);
        userRepository.save(user);
        System.out.println("Конец добавления ползователя в user.txt");
    }

    public void searchUserByAge(String dataFile) {

        UserRepository userRepository = new UserRepositoryFileImpl(dataFile);
        List<User> usersToSearchByAge = userRepository.findAll();
        int ageToSeach = 15;
        List<User> usersAge = userRepository.findByAge(usersToSearchByAge, ageToSeach);
        if (usersAge.size() > 0)
            for (User userA : usersAge) {
                System.out.println(userA.getName() + " " + userA.getAge());
            }
        else {
            System.out.println("Пользователи с возрастом " + ageToSeach + " не найдены");
        }
    }

    public void searchUserByWork(String dataFile) {

        UserRepository userRepository = new UserRepositoryFileImpl(dataFile);
        List<User> usersToSearchByWork = userRepository.findAll();
        // TODO избавится от непосредственного значения
        boolean userIsWork = true;
        List<User> usersToWork = userRepository.findByIsWorkerIsTrue(usersToSearchByWork, userIsWork);
        if (usersToWork.size() > 0)
            for (User userW : usersToWork) {
                System.out.println(userW.getIsWorker() + " " + userW.getName());
            }
        else {
            System.out.println("Работающие пользователи  не найдены");
        }
    }

    public void searchUserById(String dataFile) {

        UserRepository userRepository = new UserRepositoryFileImpl(dataFile);
        List<User> usersToSearchById = userRepository.findAll();
        // TODO избавится от непосредственного значения
        int idToSearch = 9;
        List<User> usersId = userRepository.findById(usersToSearchById, idToSearch);
        if (usersId.size() > 0)
            for (User userI : usersId) {
                System.out.println(userI.getId() + " " + userI.getName() + " " + userI.getAge() + " " + userI.getIsWorker());
            }
        else {
            System.out.println("Пользователи с ID " + idToSearch + " не найдены");
        }
    }

    public void rewriteUser(String dataFile) {

        UserRepository userRepository = new UserRepositoryFileImpl(dataFile);
        List<User> users = userRepository.findAll();

        DataProvider dp = new DataProvider();
        int idToSearch = dp.getUserID();

        List<User> userById = userRepository.findById(users, idToSearch);
        int indexById = users.indexOf(userById.get(dp.getIndexIDField()));

        User userReplace = users.get(indexById);
        userReplace.setName(dp.getUserNameForReplace());
        userReplace.setAge(dp.getUserAgeForReplace());
        userRepository.update(userReplace);

    }

}
