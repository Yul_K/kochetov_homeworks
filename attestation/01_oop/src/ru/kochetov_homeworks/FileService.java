package ru.kochetov_homeworks;

import java.io.*;

public class FileService {

    // при вызове конструктора происходит обращение
    // к операционой системе за дескриптором файла
    // там тоже может всякое случится, поэтому
    // конструктор может поднять исключение
    public FileReader OpenFileForReading(String fileName) {
        FileReader reader = null;
        try {
            reader = new FileReader(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
        return reader;
    }

    // сам BufferedReader исключения не поднимает
    // исключения возникают при методах, типа чтения, закрытия, итд
    public BufferedReader OpenBufferedReader(FileReader reader) {
        BufferedReader bufferedReader = null;
        bufferedReader = new BufferedReader(reader);
        return bufferedReader;
    }

    public FileWriter OpenFileForWriting(String fileName) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(fileName, true);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return writer;
    }

    public FileWriter OpenFileForReWriting(String fileName) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(fileName);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return writer;
    }

    public BufferedWriter OpenBufferedWriter(FileWriter writer) {
        BufferedWriter bufferedWriter = null;
        bufferedWriter = new BufferedWriter(writer);
        return bufferedWriter;
    }

    public void closeFileForReading(FileReader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException ignore) {
            }
        }
    }

    public void closeBufferedReader(BufferedReader bufferedReader) {
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException ignore) {
            }
        }
    }

    public void closeFileForWriting(FileWriter writer) {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException ignore) {
            }
        }
    }

    public void closeBufferedWriting(BufferedWriter bufferedWriter) {
        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException ignore) {
            }
        }
    }
}
