package ru.kochetov_homeworks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryFileImpl implements UserRepository {

    private String fileName;

    public UserRepositoryFileImpl(String fileName) {

        this.fileName = fileName;
    }

    @Override
    public List<User> findById(List<User> users, int id) {
        ArrayList<User> usersById = new ArrayList();
        for (User u : users) {
            if (u.getId() == id) {
                usersById.add(u);
            }
        }
        return usersById;
    }

    @Override
    public List<User> findAll() {

        List<User> users = new ArrayList<>();
        FileService f = new FileService();
        FileReader reader = f.OpenFileForReading(fileName);
        BufferedReader bufferedReader = f.OpenBufferedReader(reader);
        LineService l = new LineService();
        String line = l.LineFromReader(bufferedReader);

        while (line != null) {
            if ( !line.isBlank() ) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                User newUser = new User(id, name, age, isWorker);
                users.add(newUser);
                line = l.LineFromReader(bufferedReader);
            }
        }
        f.closeBufferedReader(bufferedReader);
        f.closeFileForReading(reader);
        return users;
    }

    @Override
    public void save(User user) {

        FileService f = new FileService();
        FileWriter writer = f.OpenFileForWriting(fileName);
        BufferedWriter bufferedWriter = f.OpenBufferedWriter(writer);
        LineService l = new LineService();
        String stringToFile = (user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.getIsWorker());
        l.LineToFile(bufferedWriter, stringToFile);
        f.closeBufferedWriting(bufferedWriter);
        f.closeFileForWriting(writer);
    }

    @Override
    public void update(User user) {

        FileService f = new FileService();
        List<User> users = findAll();

        DataProvider dp = new DataProvider();
        users.set(dp.getIndexIDField(), user);

        // Открыть файл в режиме перезаписи
        // для усечения до нуля
        FileWriter rewriter = f.OpenFileForReWriting(fileName);
        f.closeFileForWriting(rewriter);

        for (User userOut : users) {
            save(userOut);
        }
    }


    @Override
    public List<User> findByAge(List<User> users, int age) {
        ArrayList<User> usersByAge = new ArrayList();
        for (User u : users) {
            if (u.getAge() == age) {
                usersByAge.add(u);
            }

        }

        return usersByAge;
    }

    @Override
    public List<User> findByIsWorkerIsTrue(List<User> users, boolean isWorks) {
        ArrayList<User> usersByWork = new ArrayList();
        for (User u : users) {
            if (u.getIsWorker() == isWorks) {
                usersByWork.add(u);
            }

        }
        return usersByWork;
    }

}
