package ru.kochetov_homeworks;

import java.util.List;

public interface UserRepository {
    List<User> findById(List<User> usersToSearchById, int idToSeach);
    List<User> findAll();
    List<User> findByAge(List<User> usersToSearchByAge, int ageToSeach);
    List<User> findByIsWorkerIsTrue(List<User> usersToSearchByWork, boolean userIsWork);
    void save(User user);
    void update(User user);

}
