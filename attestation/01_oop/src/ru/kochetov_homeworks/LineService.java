package ru.kochetov_homeworks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class LineService {

    // При чтении с BufferedReader могут возникнуть
    // исключения
    public String LineFromReader(BufferedReader bufferedReader) {
        String line;
        try {
            line = bufferedReader.readLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return line;
    }

    public void LineToFile(BufferedWriter bufferedWriter, String stringToFile) {

        try {
            bufferedWriter.write(stringToFile);
        } catch (
                IOException e) {
            throw new IllegalArgumentException(e);
        }

        try {
            bufferedWriter.newLine();
        } catch (
                IOException e) {
            throw new IllegalArgumentException(e);
        }

        try {
            bufferedWriter.flush();
        } catch (
                IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

