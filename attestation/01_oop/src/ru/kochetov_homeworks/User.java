package ru.kochetov_homeworks;

public class User {
    private int id;
    private String name;
    private int age;
    private boolean isWorker;
    public User(int id, String name, int age, boolean isWorker){
        this.id = id;
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }
    public int getId() { return this.id; }
    public String  getName(){
        return this.name;
    }
    public int getAge() {
        return this.age;
    }
    public boolean getIsWorker(){
        return this.isWorker;
    }

    public void setName(String name){ this.name = name; }
    public void setAge(int age) {this.age = age; }

}
