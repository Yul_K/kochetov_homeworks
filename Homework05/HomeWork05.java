// Homework05
//
// Задание:
//
//Реализовать программу на Java, которая для последовательности 
//чисел, оканчивающихся на -1 выведет самую минимальную цифру, 
//встречающуюся среди чисел последовательности.
// Например:
// 345
// 298
// 456
// -1
//
// Ответ: 2
//
// Решение:
//

import java.util.Scanner;

public class HomeWork05 {

	public static void main(String[] args) {

		Scanner  s = new Scanner(System.in);

		System.out.println("Введите целое положительное число: ");
		int a = s.nextInt();
	
		int i = 1;
		int min = 1;
	
		while (a != -1) {
			System.out.println("Введите целое положительное число: ");
			int b = s.nextInt();
			if (b <= -1) {
				a = -1;
			}
			if (b == 0) {
				a = 0;
				min = i + 1;
			}
			if (b > 0){
				if (a > 0) {
					if (b < a){
						a = b;
						min = i + 1;
					}
				}
			}
			i = i + 1;
		}
		System.out.println("Минимальное число № " + min);
	
	}
}





