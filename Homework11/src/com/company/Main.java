package com.company;

/*  Задание:
 *
 * Сделать класс Logger с методом void log(String message), который выводит в консоль какое-либо сообщение.
 * Применить паттерн Singleton для Logger.
 *
 * */

// Решение

import static com.company.LogService.logger;

public class Main {

    public static void main(String[] args) {

        //LogService l = new LogService();
        String s = System.lineSeparator();
        //l.logger("Hello, world" + s);
        logger("Hello, world" + s);
    }
}
