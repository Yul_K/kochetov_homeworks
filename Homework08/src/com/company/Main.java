package com.company;

// Homework08
//
// Задание:
// На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
//
// Считать эти данные в массив объектов.
//
// Вывести в отсортированном по возрастанию веса порядке.

// Решение:


public class Main {

    public static void main(String[] args) {
        //
        String[] anNames = new String[]{"Вася", "Петя", "Слава", "Саша", "Женя", "Вова", "Серёжа", "Жора", "Маша", "Зина"};
        int[] anWeight = new int[]{52, 63, 15, 27, 78, 117, 350, 42, 36, 8};
        Human[] anHumansArray = new Human[10];
        int d;
        // ввод
        for (int i = 0; i < anHumansArray.length; i++) {
            Human hu = new Human();
            hu.setName(anNames[i]);
            hu.setWeight(anWeight[i]);
            anHumansArray[i] = hu;
        }
        // вывод
        System.out.println("Массив до сортировки: ");
        for (Human human : anHumansArray) {
            System.out.println(human.getName() + ": " + human.getWeight());
        }
        // сортировка
        for (int i = 0; i < anHumansArray.length; i++) {
            Human min = anHumansArray[i];
            int indexMin = i;
            for (int j = i; j < anHumansArray.length; j++) {
                if (anHumansArray[indexMin].getWeight() > anHumansArray[j].getWeight()) {
                    min = anHumansArray[j];
                    indexMin = j;
                }
            }
            //anHumansArray[i] = null;
            Human temp = anHumansArray[i];
            anHumansArray[i] = anHumansArray[indexMin];
            anHumansArray[indexMin] = temp;
        }
        // снова вывод
        System.out.println("Массив после сортировки: ");
        for (Human human : anHumansArray) {
            System.out.println(human.getName() + ": " + human.getWeight());
        }
    }
}




