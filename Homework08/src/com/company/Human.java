package com.company;

public class Human {
    String name = "nobody";
    int weight = -1;
    void setName(String nameIm){
        //TODO сделать проверку входног строк
        // на то что это строка, как минимум
        this.name = nameIm;
    }
    void setWeight(int weightIn){
        //TODO сделать проверку входных
        // значений на соответствие
        // диапазону от 0 до 155
        this.weight = weightIn;
    }
    String getName(){
        return this.name;
    }
    int getWeight(){
        return this.weight;
    }

}

