CREATE TABLE manager_of_customer
(
    id          UUID PRIMARY KEY,
    manager_id UUID,
    FOREIGN KEY (manager_id) REFERENCES product (id),
    customer_id UUID,
    FOREIGN KEY (customer_id) REFERENCES customer (id)


)