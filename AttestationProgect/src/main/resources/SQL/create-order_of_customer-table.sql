CREATE TABLE order_of_customer
(
    id          UUID PRIMARY KEY,
    product_id UUID,
    FOREIGN KEY (product_id) REFERENCES product (id),
    customer_id UUID,
    FOREIGN KEY (customer_id) REFERENCES customer (id)


)
