package ru.kochetov.attestationprogect.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.kochetov.attestationprogect.model.ManagerOfCustomer;
import ru.kochetov.attestationprogect.repository.ManagerOfCustomerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ManagerOfCustomerDatabaseService implements ManagerOfCustomerService {

    private final ManagerOfCustomerRepository managerOfCustomerRepository;

    @Override
    public ManagerOfCustomer getById(UUID id) {

        return  managerOfCustomerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Manager of customer with id %s not found", id)));
    }

    @Override
    public List<ManagerOfCustomer> getAll() {

        Iterable<ManagerOfCustomer> customerIterable = managerOfCustomerRepository.findAll();
        List<ManagerOfCustomer> customersList = new ArrayList<>();
        customerIterable.forEach(customersList::add);
        return customersList;
    }

    @Override
    public ManagerOfCustomer createManagerOfCustomer(ManagerOfCustomer managerOfCustomer) {

        managerOfCustomer.setId(UUID.randomUUID());
        return managerOfCustomerRepository.save(managerOfCustomer);
    }

    @Override
    public ManagerOfCustomer updateManagerOfCustomer(UUID id, ManagerOfCustomer managerOfCustomer) {

        ManagerOfCustomer managerOfCustomerFromDatabase = getById(id);
        ManagerOfCustomer managerOfCustomerToUpdate = managerOfCustomerFromDatabase.toBuilder()
                .managerId(managerOfCustomer.getManagerId())
                .customerId(managerOfCustomer.getCustomerId())
                .build();

        return managerOfCustomerRepository.save(managerOfCustomerToUpdate);
    }

    @Override
    public void delete(UUID id) {
        managerOfCustomerRepository.deleteById(id);
    }
}
