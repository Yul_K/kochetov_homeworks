package ru.kochetov.attestationprogect.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.kochetov.attestationprogect.model.Customer;
import ru.kochetov.attestationprogect.model.OrderOfCustomer;
import ru.kochetov.attestationprogect.repository.OrderOfCustomerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class OrderOfCustomerDatabaseService implements OrderOfCustomerService {

    private final OrderOfCustomerRepository orderOfCustomerRepository;

    @Override
    public OrderOfCustomer getById(UUID id) {

        return  orderOfCustomerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Order of Customer with id %s not found", id)));
    }

    @Override
    public List<OrderOfCustomer> getAll() {

        Iterable<OrderOfCustomer> orderOfCustomerIterable = orderOfCustomerRepository.findAll();
        List<OrderOfCustomer> orderOfCustomersList = new ArrayList<>();
        orderOfCustomerIterable.forEach(orderOfCustomersList::add);
        return orderOfCustomersList;
    }

    @Override
    public OrderOfCustomer createOrderOfCustomer(OrderOfCustomer orderOfCustomer) {

        orderOfCustomer.setId(UUID.randomUUID());
        return orderOfCustomerRepository.save(orderOfCustomer);
    }

    @Override
    public OrderOfCustomer updateOrderOfCustomer(UUID id, OrderOfCustomer orderOfCustomer) {

        OrderOfCustomer orderOfCustomerFromDatabase = getById(id);
        OrderOfCustomer orderOfCustomerToUpdate = orderOfCustomerFromDatabase.toBuilder()
                .prodictId(orderOfCustomer.getProdictId())
                .customerId(orderOfCustomer.getCustomerId())
                .build();

        return orderOfCustomerRepository.save(orderOfCustomerToUpdate);
    }

    @Override
    public void delete(UUID id) {

        orderOfCustomerRepository.deleteById(id);
    }
}
