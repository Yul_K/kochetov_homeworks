package ru.kochetov.attestationprogect.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.kochetov.attestationprogect.model.Customer;
import ru.kochetov.attestationprogect.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class CustomerDatabaseService implements CustomerService {

    private final CustomerRepository customerRepository;

    @Override
    public Customer getById(UUID id) {

        return  customerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Customer with id %s not found", id)));
    }

    @Override
    public List<Customer> getAll() {

        Iterable<Customer> customerIterable = customerRepository.findAll();
        List<Customer> customersList = new ArrayList<>();
        customerIterable.forEach(customersList::add);
        return customersList;
    }

    @Override
    public Customer createCustomer(Customer customer) {

        customer.setId(UUID.randomUUID());
        return customerRepository.save(customer);
    }

    @Override
    public Customer updateCustomer(UUID id, Customer customer) {

        Customer customerFromDatabase = getById(id);
        Customer customerToUpdate = customerFromDatabase.toBuilder()
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .build();

        return customerRepository.save(customerToUpdate);
    }

    @Override
    public void delete(UUID id) {

        customerRepository.deleteById(id);
    }
}
