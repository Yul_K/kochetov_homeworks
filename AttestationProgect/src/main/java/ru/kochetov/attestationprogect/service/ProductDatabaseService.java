package ru.kochetov.attestationprogect.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.kochetov.attestationprogect.model.Customer;
import ru.kochetov.attestationprogect.model.Product;
import ru.kochetov.attestationprogect.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ProductDatabaseService implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public Product getById(UUID id) {

        return  productRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Product with id %s not found", id)));
    }

    @Override
    public List<Product> getAll() {

        Iterable<Product> productIterable = productRepository.findAll();
        List<Product> productsList = new ArrayList<>();
        productIterable.forEach(productsList::add);
        return productsList;
    }

    @Override
    public Product createProduct(Product product) {

        product.setId(UUID.randomUUID());
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(UUID id, Product product) {

        Product productFromDatabase = getById(id);
        Product productToUpdate = productFromDatabase.toBuilder()
                .nameProduct(product.getNameProduct())
                .descriptionProduct(product.getDescriptionProduct())
                .priceProduct(product.getPriceProduct())
                .numberProduct(product.getNumberProduct())
                .build();

        return productRepository.save(productToUpdate);
    }

    @Override
    public void delete(UUID id) {

        productRepository.deleteById(id);
    }
}
