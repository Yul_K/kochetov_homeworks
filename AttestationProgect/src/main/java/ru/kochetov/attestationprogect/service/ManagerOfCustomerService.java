package ru.kochetov.attestationprogect.service;

import ru.kochetov.attestationprogect.model.ManagerOfCustomer;

import java.util.List;
import java.util.UUID;

public interface ManagerOfCustomerService {

    ManagerOfCustomer getById(UUID id);
    List<ManagerOfCustomer> getAll();
    ManagerOfCustomer createManagerOfCustomer(ManagerOfCustomer managerOfCustomer);
    ManagerOfCustomer updateManagerOfCustomer(UUID id, ManagerOfCustomer managerOfCustomer);
    void delete(UUID id);
}
