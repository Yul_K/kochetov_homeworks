package ru.kochetov.attestationprogect.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.kochetov.attestationprogect.model.Manager;
import ru.kochetov.attestationprogect.repository.ManagerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ManagerDatabaseService implements ManagerService {

    private final ManagerRepository managerRepository;

    @Override
    public Manager getById(UUID id) {

        return  managerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Manager with id %s not found", id)));
    }

    @Override
    public List<Manager> getAll() {

        Iterable<Manager> managerIterable = managerRepository.findAll();
        List<Manager> managersList = new ArrayList<>();
        managerIterable.forEach(managersList::add);
        return managersList;
    }

    @Override
    public Manager createManager(Manager manager) {

        manager.setId(UUID.randomUUID());
        return managerRepository.save(manager);
    }

    @Override
    public Manager updateManager(UUID id, Manager manager) {

        Manager managerFromDatabase = getById(id);
        Manager managerToUpdate = managerFromDatabase.toBuilder()
                .firstName(manager.getFirstName())
                .lastName(manager.getLastName())
                .build();

        return managerRepository.save(managerToUpdate);
    }

    @Override
    public void delete(UUID id) {

        managerRepository.deleteById(id);
    }
}
