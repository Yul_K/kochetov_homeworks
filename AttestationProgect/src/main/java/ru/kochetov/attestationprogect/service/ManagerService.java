package ru.kochetov.attestationprogect.service;

import ru.kochetov.attestationprogect.model.Manager;

import java.util.List;
import java.util.UUID;

public interface ManagerService {

    Manager getById(UUID id);
    List<Manager> getAll();
    Manager createManager(Manager manager);
    Manager updateManager(UUID id, Manager manager);
    void delete(UUID id);
}
