package ru.kochetov.attestationprogect.service;

import ru.kochetov.attestationprogect.model.Product;

import java.util.List;
import java.util.UUID;

public interface ProductService {

    Product getById(UUID id);
    List<Product> getAll();
    Product createProduct(Product product);
    Product updateProduct(UUID id, Product product);
    void delete(UUID id);
}
