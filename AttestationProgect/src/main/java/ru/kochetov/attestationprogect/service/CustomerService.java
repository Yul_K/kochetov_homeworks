package ru.kochetov.attestationprogect.service;

import ru.kochetov.attestationprogect.model.Customer;

import java.util.List;
import java.util.UUID;

public interface CustomerService {

    Customer getById(UUID id);
    List<Customer> getAll();
    Customer createCustomer(Customer customer);
    Customer updateCustomer(UUID id, Customer customer);
    void delete(UUID id);
}
