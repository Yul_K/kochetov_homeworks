package ru.kochetov.attestationprogect.service;

import ru.kochetov.attestationprogect.model.OrderOfCustomer;

import java.util.List;
import java.util.UUID;

public interface OrderOfCustomerService {

    OrderOfCustomer getById(UUID id);
    List<OrderOfCustomer> getAll();
    OrderOfCustomer createOrderOfCustomer(OrderOfCustomer orderOfCustomer);
    OrderOfCustomer updateOrderOfCustomer(UUID id, OrderOfCustomer orderOfCustomer);
    void delete(UUID id);
}
