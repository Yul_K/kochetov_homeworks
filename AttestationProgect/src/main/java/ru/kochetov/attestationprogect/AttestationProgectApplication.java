package ru.kochetov.attestationprogect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AttestationProgectApplication {

    public static void main(String[] args) {
        SpringApplication.run(AttestationProgectApplication.class, args);
    }

}
