package ru.kochetov.attestationprogect.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kochetov.attestationprogect.model.Manager;
import ru.kochetov.attestationprogect.service.ManagerService;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/manager")
@RequiredArgsConstructor

public class ManagerController {

    private final ManagerService managerServices;

    @GetMapping
    public String managerPage(Model model) {
        List<Manager> managers = managerServices.getAll();
        model.addAttribute("managers", managers);
        return "managers/managers";
    }

    @GetMapping("/{id}")
    public String managerPage(Model model, @PathVariable UUID id) {
        Manager manager = managerServices.getById(id);
        model.addAttribute("manager", manager);
        return "managers/manager";
    }

    @GetMapping("/new")
    public String managerCreatePage(Model model) {
        model.addAttribute("manager", new Manager());
        return "managers/manager";
    }

    @PostMapping("/delete/{id}")
    public String deleteManager(@PathVariable UUID id) {
        managerServices.delete(id);
        return "redirect:/manager";
    }

    @PostMapping
    public String createOrUpdateManager(@ModelAttribute("userDto") Manager userDto) {
        if (userDto.getId() == null) {
            managerServices.createManager(userDto);
        } else {
            managerServices.updateManager(userDto.getId(), userDto);
        }

        return "redirect:/manager";
    }
}
