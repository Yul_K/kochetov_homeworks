package ru.kochetov.attestationprogect.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kochetov.attestationprogect.model.OrderOfCustomer;
import ru.kochetov.attestationprogect.service.OrderOfCustomerService;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/orderOfCustomer")
@RequiredArgsConstructor

public class OrderOfCustomerController {

    private final OrderOfCustomerService orderOfCustomerServices;

    @GetMapping
    public String orderOfCustomerPage(Model model) {
        List<OrderOfCustomer> ordersOfCustomers = orderOfCustomerServices.getAll();
        model.addAttribute("ordersOfCustomers", ordersOfCustomers);
        return "ordersOfCustomers/ordersOfCustomers";
    }

    @GetMapping("/{id}")
    public String orderOfCustomerPage(Model model, @PathVariable UUID id) {
        OrderOfCustomer orderOfCustomer = orderOfCustomerServices.getById(id);
        model.addAttribute("managerOfCustomer", orderOfCustomer);
        return "ordersOfCustomers/orderOfCustomer";
    }

    @GetMapping("/new")
    public String orderOfCustomerCreatePage(Model model) {
        model.addAttribute("orderOfCustomer", new OrderOfCustomer());
        return "ordersOfCustomers/orderOfCustomer";
    }

    @PostMapping("/delete/{id}")
    public String deleteOrderOfCustomer(@PathVariable UUID id) {
        orderOfCustomerServices.delete(id);
        return "redirect:/orderOfCustomer";
    }

    @PostMapping
    public String createOrUpdateOrderOfCustomer(@ModelAttribute("userDto") OrderOfCustomer userDto) {
        if (userDto.getId() == null) {
            orderOfCustomerServices.createOrderOfCustomer(userDto);
        } else {
            orderOfCustomerServices.updateOrderOfCustomer(userDto.getId(), userDto);
        }

        return "redirect:/orderOfCustomer";
    }

}
