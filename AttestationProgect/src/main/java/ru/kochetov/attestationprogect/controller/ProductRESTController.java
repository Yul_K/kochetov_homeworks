package ru.kochetov.attestationprogect.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.kochetov.attestationprogect.model.Product;
import ru.kochetov.attestationprogect.service.ProductService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/rest_product")
@RequiredArgsConstructor


public class ProductRESTController {

    private final ProductService productServices;

    @GetMapping
    public List<Product> getAllProduct() {
        return productServices.getAll();
    }

    @GetMapping("/{id}")
    public Product getById(@PathVariable UUID id) {
        return productServices.getById(id);
    }

    @PostMapping
    public Product createProduct(@RequestBody Product product) {
        return productServices.createProduct(product);
    }

    @PutMapping("/{id}")
    public Product updateCustomer(@PathVariable UUID id, @RequestBody Product product) {
        return productServices.updateProduct(id, product);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        productServices.delete(id);
    }
}

