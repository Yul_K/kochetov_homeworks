package ru.kochetov.attestationprogect.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import ru.kochetov.attestationprogect.model.Product;
import ru.kochetov.attestationprogect.service.ProductService;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/product")
@RequiredArgsConstructor

public class ProductController {

    private final ProductService productService;

    @GetMapping
    public String productPage(Model model) {
        List<Product> products = productService.getAll();
        model.addAttribute("products", products);
        return "products/prodicts";
    }

    @GetMapping("/{id}")
    public String productPage(Model model, @PathVariable UUID id) {
        Product product = productService.getById(id);
        model.addAttribute("product", product);
        return "products/product";
    }

    @GetMapping("/new")
    public String prodictCreatePage(Model model) {
        model.addAttribute("product", new Product());
        return "products/prodict";
    }

    @PostMapping("/delete/{id}")
    public String deleteProduct(@PathVariable UUID id) {
        productService.delete(id);
        return "redirect:/product";
    }

    @PostMapping
    public String createOrUpdateProduct(@ModelAttribute("userDto") Product userDto) {
        if (userDto.getId() == null) {
            productService.createProduct(userDto);
        } else {
            productService.updateProduct(userDto.getId(), userDto);
        }

        return "redirect:/product";
    }

}
