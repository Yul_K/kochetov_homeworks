package ru.kochetov.attestationprogect.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.kochetov.attestationprogect.model.Manager;
import ru.kochetov.attestationprogect.service.ManagerService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/rest_manager")
@RequiredArgsConstructor


public class ManagerRESTController {

    private final ManagerService managerServices;

    @GetMapping
    public List<Manager> getAllManager() {
        return managerServices.getAll();
    }

    @GetMapping("/{id}")
    public Manager getById(@PathVariable UUID id) {
        return managerServices.getById(id);
    }

    @PostMapping
    public Manager createManager(@RequestBody Manager manager) {
        return managerServices.createManager(manager);
    }

    @PutMapping("/{id}")
    public Manager updateManager(@PathVariable UUID id, @RequestBody Manager manager) {
        return managerServices.updateManager(id, manager);
    }

    @DeleteMapping("/{id}")
    public void deleteManager(@PathVariable UUID id) {
        managerServices.delete(id);
    }
}

