package ru.kochetov.attestationprogect.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kochetov.attestationprogect.model.Customer;
import ru.kochetov.attestationprogect.service.CustomerService;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/customer")
@RequiredArgsConstructor

public class CustomerController {

    private final CustomerService customerServices;

    @GetMapping
    public String customerPage(Model model) {
        List<Customer> customers = customerServices.getAll();
        model.addAttribute("customers", customers);
        return "customers/customers";
    }

    @GetMapping("/{id}")
    public String customerPage(Model model, @PathVariable UUID id) {
        Customer customer = customerServices.getById(id);
        model.addAttribute("customer", customer);
        return "customers/customer";
    }

    @GetMapping("/new")
    public String customerCreatePage(Model model) {
        model.addAttribute("customer", new Customer());
        return "customers/customer";
    }

    @PostMapping("/delete/{id}")
    public String deleteCustomer(@PathVariable UUID id) {
        customerServices.delete(id);
        return "redirect:/customer";
    }

    @PostMapping
    public String createOrUpdateCustomer(@ModelAttribute("userDto") Customer userDto) {
        if (userDto.getId() == null) {
            customerServices.createCustomer(userDto);
        } else {
            customerServices.updateCustomer(userDto.getId(), userDto);
        }

        return "redirect:/customer";
    }
}
