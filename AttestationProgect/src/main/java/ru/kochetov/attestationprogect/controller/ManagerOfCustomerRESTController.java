package ru.kochetov.attestationprogect.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kochetov.attestationprogect.model.ManagerOfCustomer;
import ru.kochetov.attestationprogect.service.ManagerOfCustomerService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/rest_managerOfCustomer")
@RequiredArgsConstructor


public class ManagerOfCustomerRESTController {

    private final ManagerOfCustomerService managerOfCustomerServices;

    @GetMapping
    public List<ManagerOfCustomer> getAllManagersOfCustomer() {
        return managerOfCustomerServices.getAll();
    }

    @GetMapping("/{id}")
    public ManagerOfCustomer getById(@PathVariable UUID id) {
        return managerOfCustomerServices.getById(id);
    }

    @PostMapping
    public ManagerOfCustomer createManagerOfCustomer(@RequestBody ManagerOfCustomer managerOfCustomer) {
        return managerOfCustomerServices.createManagerOfCustomer(managerOfCustomer);
    }

    @PutMapping("/{id}")
    public ManagerOfCustomer updateManager(@PathVariable UUID id, @RequestBody ManagerOfCustomer managerOfCustomer) {
        return managerOfCustomerServices.updateManagerOfCustomer(id, managerOfCustomer);
    }

    @DeleteMapping("/{id}")
    public void deleteManager(@PathVariable UUID id) {
        managerOfCustomerServices.delete(id);
    }
}

