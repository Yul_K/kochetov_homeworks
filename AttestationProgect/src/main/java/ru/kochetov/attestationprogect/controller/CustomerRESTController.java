package ru.kochetov.attestationprogect.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kochetov.attestationprogect.model.Customer;
import ru.kochetov.attestationprogect.service.CustomerService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/rest_customer")
@RequiredArgsConstructor


public class CustomerRESTController {

    private final CustomerService customerServices;

    @GetMapping
    public List<Customer> getAllCustomer() {
        return customerServices.getAll();
    }

    @GetMapping("/{id}")
    public Customer getById(@PathVariable UUID id) {
        return customerServices.getById(id);
    }

    @PostMapping
    public Customer createCustomer(@RequestBody Customer customer) {
        return customerServices.createCustomer(customer);
    }

    @PutMapping("/{id}")
    public Customer updateCustomer(@PathVariable UUID id, @RequestBody Customer customer) {
        return customerServices.updateCustomer(id, customer);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        customerServices.delete(id);
    }
}

