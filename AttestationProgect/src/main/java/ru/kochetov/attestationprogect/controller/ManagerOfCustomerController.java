package ru.kochetov.attestationprogect.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kochetov.attestationprogect.model.ManagerOfCustomer;
import ru.kochetov.attestationprogect.service.ManagerOfCustomerService;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/managerOfCustomer")
@RequiredArgsConstructor

public class ManagerOfCustomerController {

    private final ManagerOfCustomerService managerOfCustomerServices;

    @GetMapping
    public String managerOfCustomerPage(Model model) {
        List<ManagerOfCustomer> managersOfCustomers = managerOfCustomerServices.getAll();
        model.addAttribute("managersOfCustomers", managersOfCustomers);
        return "managersOfCustomers/managersOfCustomers";
    }

    @GetMapping("/{id}")
    public String managerOfCustomerPage(Model model, @PathVariable UUID id) {
        ManagerOfCustomer managerOfCustomer = managerOfCustomerServices.getById(id);
        model.addAttribute("managerOfCustomer", managerOfCustomer);
        return "managersOfCustomers/managerOfCustomer";
    }

    @GetMapping("/new")
    public String managerOfCustomerCreatePage(Model model) {
        model.addAttribute("managerOfCustomer", new ManagerOfCustomer());
        return "managersOfCustomers/managerOfCustomer";
    }

    @PostMapping("/delete/{id}")
    public String deleteManagerOfCustomer(@PathVariable UUID id) {
        managerOfCustomerServices.delete(id);
        return "redirect:/managerOfCustomer";
    }

    @PostMapping
    public String createOrUpdateManagerOfCustomer(@ModelAttribute("userDto") ManagerOfCustomer userDto) {
        if (userDto.getId() == null) {
            managerOfCustomerServices.createManagerOfCustomer(userDto);
        } else {
            managerOfCustomerServices.updateManagerOfCustomer(userDto.getId(), userDto);
        }

        return "redirect:/managerOfCustomer";
    }
}
