package ru.kochetov.attestationprogect.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.kochetov.attestationprogect.model.OrderOfCustomer;
import ru.kochetov.attestationprogect.service.OrderOfCustomerService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/rest_orderOfCustomer")
@RequiredArgsConstructor


public class OrderOfCustomerRESTController {

    private final OrderOfCustomerService orderOfCustomerServices;

    @GetMapping
    public List<OrderOfCustomer> getAllOrderOfCustomer() {
        return orderOfCustomerServices.getAll();
    }

    @GetMapping("/{id}")
    public OrderOfCustomer getById(@PathVariable UUID id) {
        return orderOfCustomerServices.getById(id);
    }

    @PostMapping
    public OrderOfCustomer createOrderOfCustomer(@RequestBody OrderOfCustomer orderOfCustomer) {
        return orderOfCustomerServices.createOrderOfCustomer(orderOfCustomer);
    }

    @PutMapping("/{id}")
    public OrderOfCustomer updateOrderOfCustomer(@PathVariable UUID id, @RequestBody OrderOfCustomer orderOfCustomer) {
        return orderOfCustomerServices.updateOrderOfCustomer(id, orderOfCustomer);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        orderOfCustomerServices.delete(id);
    }
}

