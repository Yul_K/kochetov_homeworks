package ru.kochetov.attestationprogect.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kochetov.attestationprogect.model.ManagerOfCustomer;

import java.util.UUID;

@Repository
public interface ManagerOfCustomerRepository extends CrudRepository<ManagerOfCustomer, UUID> {

}
