package ru.kochetov.attestationprogect.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kochetov.attestationprogect.model.Manager;

import java.util.UUID;

@Repository
public interface ManagerRepository extends CrudRepository<Manager, UUID> {

}
