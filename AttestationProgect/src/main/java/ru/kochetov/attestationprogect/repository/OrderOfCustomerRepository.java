package ru.kochetov.attestationprogect.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kochetov.attestationprogect.model.OrderOfCustomer;

import java.util.UUID;

@Repository
public interface OrderOfCustomerRepository extends CrudRepository<OrderOfCustomer, UUID> {

}
