package ru.kochetov.attestationprogect.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(of= "id")

public class Manager {
    @Id
    private UUID id;
    private String firstName;
    private String lastName;
}
