package com.company;

/*
 *  Задание:
 *
 * На вход подается последовательность чисел, оканчивающихся на -1.
 *
 * Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
 *
 * Гарантируется:
 *
 * Все числа в диапазоне от -100 до 100.
 *
 * Числа встречаются не более 2 147 483 647-раз каждое.
 *
 * Сложность алгоритма - O(n)
 */

// Решение:

import java.util.Random;
import java.util.Arrays;
import java.lang.reflect.Array;


public class Main {

    public static void main(String[] args) {

        // Мутим исходные данные
        // Использование геннератора случайных чисел
        // Первый параметр, потребное количество чисел
        // Второй, наименьшее значение генерируеммых чисел, включительно
        // Третий, наибольшее значение, не включительно
        // int NUMS = 50;
        // int MIN = 0;
        // int MAX = 10;
        // int[] anNumbers = new Random().ints(NUMS, MIN, MAX).toArray();
        // anNumbers[NUMS] = -1;

        // проверочный массив чисел
        int[] anNumbers = {6, 7, 7, 9, 6, 1, 9, 2, 5, 2, 6, 3, 5, 3, 0, 9, 1, 8, 8,
                8, 8, 6, 1, 9, 1, 2, 4, 1, 6, 0, 3, 4, 4, 5, 7, 5,
                4, 8, 6, 1, 1, 4, 9, 5, 4, 1, 0, 5, 3, 8, -1};

        // Смотрим исходных данных
        System.out.println("Исходный массив: ");
        System.out.println(Arrays.toString(anNumbers));
        //System.out.println("Длинна массива данных:" + Array.getLength(anNumbers));

        // Сортирую массив по возрастанию
        for (int i = 0; anNumbers[i] != -1; i++) {
            int min = anNumbers[i];
            int indexMin = i;
            for (int j = i; anNumbers[j] != -1; j++) {
                if (min > anNumbers[j]) {
                    min = anNumbers[j];
                    indexMin = j;
                }
            }
            anNumbers[indexMin] = anNumbers[i];
            anNumbers[i] = min;
        }

        // Смотрим отсортированные данные
        System.out.println("Отсортированый массив: ");
        System.out.println(Arrays.toString(anNumbers));


        int i = 0;
        int countNum;
        int minCountNum = 1;
        int minDigit = 0;
        while (anNumbers[i] != -1) {
            countNum = 1;
            while (anNumbers[i] == anNumbers[i + countNum]) {
                countNum++;
            }
            if (i == 0) {
                minCountNum = countNum;
            }
            if (minCountNum > countNum) {
                minCountNum = countNum;
                minDigit = anNumbers[i];
            }

            //System.out.print("Число: " + anNumbers[i]);
            //System.out.println(" Имеет вхождений: " + countNum);
            if ((i + countNum) < Array.getLength(anNumbers)) {
                i = i + countNum;
            }
        }
        System.out.println("Текущий минимум вхождений: " + minCountNum + " числа " + minDigit);
    }
}