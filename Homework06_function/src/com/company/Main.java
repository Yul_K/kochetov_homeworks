// Homework06
//
// Задание:
//
// Реализовать функцию, принимающую на вход массив и целое число.
// Данная функция должна вернуть индекс этого числа в массиве.
// Если число в массиве отсутствует - вернуть -1.
//
// Решение:
//
package com.company;

import java.util.Scanner;

public class Main {

    public static int getIndexNum(int[] anArrayIn,int Num){
        int i = 0;
        while (i < anArrayIn.length) {
            if (Num == anArrayIn[i]) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public static void main (String[] args){
        int[] anArray = new int[]{34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println("Введите целое положительное число: ");
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int outNum = getIndexNum(anArray, a);
        if (outNum == -1) {
            System.out.println("Нет заданого числа в массиве (-1)");
        } else {
            System.out.println("Индех заданого числа: " + outNum);
        }
    }
}
