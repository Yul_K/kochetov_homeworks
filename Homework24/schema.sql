//language = SQL

create table product
(
    prodict_id             serial primary key,
    name_of_product        varchar(20),
    price_of_product       integer,
    number_of_product      integer,
    description_of_product varchar(60)

);

create table customer
(
    customer_id serial primary key,
    first_name  varchar(20),
    last_name   varchar(20)
);

create table table_of_order
(
    order_id    serial primary key,
    product_id  integer,
    foreign key (product_id) references product (product_id),
    customer_id integer,
    foreign key (customer_id) references customer (customer_id)
);

insert into product (name_of_product, price_of_product, number_of_product, description_of_product)
    value ('Веник', 20, 32, 'Устройство для сбора пыли');

insert into product (name_of_product, price_of_product, number_of_product, description_of_product)
    values ('Ведро', 65, 4, 'Устройство для переноски воды');

insert into product (name_of_product, price_of_product, number_of_product, description_of_product)
    values ('Мыло', 15, 123, 'Средство для образования пены');

insert into customer (first_name, last_name)
values ('Иван', 'Иванов');

insert into customer (first_name, last_name)
values ('Сидор', 'Сидоров');

insert into customer (first_name, last_name)
values ('Петр', 'Петров');

select *
from product;

select *
from customer;

select first_name
from customer;

insert into table_of_order (product_id, customer_id)
values ('Веник', 'Петров');

select customer_id
from customer
where first_name = 'Петр'
  and last_name = 'Петров';

select product_id
from product
where name_of_product = 'Веник';

insert into table_of_order (product_id, customer_id)
values ((select product_id
         from product
         where name_of_product = 'Веник'),
        (select customer_id
         from customer
         where first_name = 'Петр'
           and last_name = 'Петров'));

insert into table_of_order (product_id, customer_id)
values (
           (select product_id
            from product
            where name_of_product = 'Ведро'),
           (select customer_id
            from customer
            where first_name = 'Сидор'
              and last_name = 'Сидоров')
       );

insert into table_of_order (product_id, customer_id)
values (
           (select product_id
            from product
            where name_of_product = 'Мыло'),
           (select customer_id
            from customer
            where first_name = 'Иван'
              and last_name = 'Иванов')
       );






