package ru.kochetov_homeworks;

public interface ByCondition {
     boolean isOk(int number);
}
