package ru.kochetov_homeworks;


public class Sequence {
    public static boolean[] filter(int[] ar, ByCondition condition) {
        boolean[] anResultArray = new boolean[ar.length];
        for (int i = 0; i < ar.length; i++) {
            anResultArray[i] = condition.isOk(ar[i]);
        }
        return anResultArray;
    }
}

