package ru.kochetov_homeworks;

/* Задание:
    Предусмотреть функциональный интерфейс

interface ByCondition {
	boolean isOk(int number);
}

Реализовать в классе Sequence метод:

public static int[] filter(int[] array, ByCondition condition) {
	...
}

Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.

В main в качестве condition подставить:

- проверку на четность элемента
- проверку, является ли сумма цифр элемента четным числом.

 */

// Решение:

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

	int[] anNumders = {10, 502, 13, 78, 467, 27};
    boolean[] anResultOfFilter = Sequence.filter(anNumders, number -> number % 2==0);
    System.out.println("Исходный массив: ");
    System.out.println(Arrays.toString(anNumders));
    System.out.println("Четность элементов массива: ");
    System.out.println(Arrays.toString(anResultOfFilter));

    SumDigit sd = new SumDigit();
    int[] anSumDigit = new int[anNumders.length];
    for (int i = 0; i < anNumders.length; i++){
        anSumDigit[i] = sd.getSum(anNumders[i]);
    }
    System.out.println("Массив сумм исходного массива: ");
    System.out.println(Arrays.toString(anSumDigit));
    boolean[] anResultOfFilterSumDigit = Sequence.filter(anSumDigit, number -> number % 2==0);
    System.out.println("Четность элементов массива сумм: ");
    System.out.println(Arrays.toString(anResultOfFilterSumDigit));

    }
}
