package ru.kochetov_homeworks;

public class SumDigit {
    int getSum(int number) {
        int sum = 0;
        while (number > 0) {
            sum = sum + number % 10;
            number = number / 10;
        }
        return sum;
    }
}
